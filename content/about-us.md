---
title: About Us
subtitle: The people that are proudly organizing this Devroom 🤓❤
date: "2019-10-08"
comments: false
---

  - Tonći Galić <a href="https://gitlab.com/Tuxified"><i class="fab fa-gitlab fa-lg" style="color:#FC6D27"></i></a> <a href="https://github.com/tuxified"><i class="fab fa-github fa-lg" style="color:black"></i></a> <a href="https://twitter.com/tuxified"><i class="fab fa-twitter fa-lg" style="color:#1DA1F2"></i></a>
  - <a href="https://about.me/benoitc">Benoît Chesneau</a> <a href="https://gitlab.com/benoitc"><i class="fab fa-gitlab fa-lg" style="color:#FC6D27"></i></a> <a href="https://github.com/benoitc"><i class="fab fa-github fa-lg" style="color:black"></i></a> <a href="https://twitter.com/benoitc"><i class="fab fa-twitter fa-lg" style="color:#1DA1F2"></i></a>
  - <a href="https://rbino.com">Riccardo Binetti</a> <a href="https://github.com/rbino"><i class="fab fa-github fa-lg" style="color:black"></i></a> <a href="https://twitter.com/errebino"><i class="fab fa-twitter fa-lg" style="color:#1DA1F2"></i></a>
  - <a href="https://blog.uninstall.it/">Davide Bettio</a> <a href="https://github.com/bettio"><i class="fab fa-github fa-lg" style="color:black"></i></a> <a href="https://twitter.com/uninstall"><i class="fab fa-twitter fa-lg" style="color:#1DA1F2"></i></a>
  - <a href="https://dnlserrano.dev/">Daniel Serrano</a> <a href="https://gitlab.com/dnlserrano"><i class="fab fa-gitlab fa-lg" style="color:#FC6D27"></i></a> <a href="https://github.com/dnlserrano"><i class="fab fa-github fa-lg" style="color:black"></i></a> <a href="https://twitter.com/dnlserrano"><i class="fab fa-twitter fa-lg" style="color:#1DA1F2"></i></a>
  - <a href="https://caixinha.pt/">Daniel Caixinha</a> <a href="https://gitlab.com/dcaixinha/"><i class="fab fa-gitlab fa-lg" style="color:#FC6D27"></i></a> <a href="https://github.com/dcaixinha"><i class="fab fa-github fa-lg" style="color:black"></i></a>

If you'd like to get in touch with us, mail us at *beam-devroom[at]lists.fosdem.org*.
