## Welcome!

At FOSDEM 2020 we're having the 1st edition of a Devroom completely dedicated to the BEAM (and all languages running on it). [FOSDEM](https://fosdem.org/) is an annual conference about free and open source software, attended by over 5000 developers and open-source enthusiasts from all over the world.

The Devroom will take place on Saturday, 1 February 2020, at [ULB (Campus Solbosch)](https://www.openstreetmap.org/node/1632534522), in Brussels, Belgium. Join us to enjoy interesting talks, demos and discussions about Erlang, Elixir and the wonderful BEAM!

The schedule is already live – [click here]({{< ref "schedule.md" >}}) to check it! We hope to meet you in Brussels 🇧🇪
